# Lua component

Experimental Lua component for Gambas

## Properties


## Example
![](docs/img01.png)

```vb
' Gambas class file

Public Sub BtnExecutar_Click()
  Lua1.Code = TextArea1.Text
  Lua1.Execute
End

Public Sub Lua1_onExecute()
  Print "Executed"
End
```

## Install
Execute 
```bash
cd $GAMBAS_ROOT/gb.lua
make
sudo make install
```

## Development


```bash
sudo ln -s $GAMBAS_ROOT/gb.lua/src/gb.lua.component .
sudo ln -s $GAMBAS_ROOT/gb.lua/src/.libs/gb.lua.la .
sudo ln -s $GAMBAS_ROOT/gb.lua/src/.libs/gb.lua.so .
sudo ln -s $GAMBAS_ROOT/gb.lua/src/.libs/gb.lua.so.0 .
sudo ln -s $GAMBAS_ROOT/gb.lua/src/.libs/gb.lua.so.0.0.0 .
```

## To do
- Input/Output events in component
- Batteries included
- Documentation
