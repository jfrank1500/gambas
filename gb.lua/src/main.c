/***************************************************************************

main.c

gb.lua component

(c) 2023 Frank <jfrank1500@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 1, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.

***************************************************************************/

#define __MAIN_C

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "gb_common.h"

#define THIS ((CLUA *)_object)

#define IMPLEMENT_STRING(_name, _prop)             \
  BEGIN_PROPERTY(Lua_##_name)                      \
  if (READ_PROPERTY)                               \
    GB.ReturnString(THIS->_prop);                  \
  else                                             \
    GB.StoreString(PROP(GB_STRING), &THIS->_prop); \
  END_PROPERTY

#define IMPLEMENT_INTEGER(_name, _prop) \
  BEGIN_PROPERTY(Lua_##_name)           \
  if (READ_PROPERTY)                    \
    GB.ReturnInteger(THIS->_prop);      \
  else                                  \
    THIS->_prop = VPROP(GB_INTEGER);    \
                                        \
  END_PROPERTY

GB_INTERFACE GB EXPORT;

int EXPORT GB_INIT(void)
{
  return 0;
}

void EXPORT GB_EXIT()
{
}

DECLARE_EVENT(EVENT_Execute);

BEGIN_METHOD_VOID(Lua_new)
THIS->version = 10;
THIS->L = luaL_newstate();
luaL_openlibs(THIS->L);
END_METHOD

BEGIN_METHOD_VOID(Lua_free)
lua_close(THIS->L);
END_METHOD

BEGIN_METHOD_VOID(Lua_execute)
if (THIS->code)
{
  luaL_dostring(THIS->L, THIS->code);
}
GB.Raise(THIS, EVENT_Execute, 0);
END_METHOD

IMPLEMENT_INTEGER(Version, version)
IMPLEMENT_STRING(Code, code)

GB_DESC LuaDesc[] = {
    GB_DECLARE("_Lua", sizeof(CLUA)),
    GB_METHOD("_new", NULL, Lua_new, NULL),
    GB_METHOD("_free", NULL, Lua_free, NULL),
    GB_METHOD("Execute", NULL, Lua_execute, NULL),
    GB_PROPERTY("Version", "i", Lua_Version),
    GB_PROPERTY("Code", "s", Lua_Code),
    GB_CONSTANT("_IsControl", "b", TRUE),
    GB_CONSTANT("_Family", "s", "Form"),
    GB_CONSTANT("_IsVirtual", "b", TRUE),
    GB_CONSTANT("_Group", "s", "Special"),
    GB_CONSTANT("_Properties", "s", "Code,Version=1"),
    GB_CONSTANT("_DefaultEvent", "s", "onExecute"),
    GB_EVENT("onExecute", NULL, NULL, &EVENT_Execute),
    GB_END_DECLARE};

GB_DESC *GB_CLASSES[] EXPORT = {
    LuaDesc,
    NULL};
